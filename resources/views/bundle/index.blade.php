@extends('template.layout')
@section('main-container')
<div class="row">
    <div class="col-xs-12" style="margin-top: 20px;">
        <form autocomplete="off" class="form-horizontal">
            <div class="box box-solid">
                <div class="box-body" style="padding: 25px;">
                    <h4 style="margin: 0px;">Cari Product Bundle</h4>
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 20px;">
                            <div class="form-group">
                                <span class="input-group"><input class="form-control input-lg form-control" id="filter" name="filter" placeholder="Cari &quot;iPhone 7&quot;" type="text" value=""><span class="input-group-btn"><button class="btn btn-lg btn-primary" id="btnSearchProduct" type="submit"><i class="fa fa-search"></i></button></button></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="box box-solid">
            <div class="box-body">
                @foreach($brands as $key => $value)
                <div>{{$value['_id']}}</div>
                @endforeach
                <table class="table table-bordered">
                    <thead class="thead-default">
                        <tr>
                            <th>#</th>
                            <th>Rules</th>
                            <th>Type</th>
                            <th>Value</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Active?</th>
                            <th>Editor</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div> 
    </div>
</div>
@endsection