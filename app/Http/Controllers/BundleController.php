<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use URL;

class BundleController extends Controller
{
    public function index() {

        $string = file_get_contents(URL('/static/brands.json'));
        $brands = json_decode($string, true);
        // print_r($brands);
        // die();
        $data = array (
            "message" => "Hallo bundle",
            "brands" => $brands
        );
        return view('bundle.index')->with($data);
    }
}
